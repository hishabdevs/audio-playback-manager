import logging

from flask import Flask, jsonify, request

from modules.audio_generator import call_audio_generator
from modules.audio_player import wav_player
from modules.db_helpers import sqlite_helper

FLASK_HOST = '0.0.0.0'
FLASK_PORT = 8000

app = Flask(__name__)

@app.route('/call_started', methods=['GET', ])
def call_started():
    call_wav_path, call_id, audio_duration = call_audio_generator\
        .generate_wav_file_for_call(max_utt_count=5)

    logging.info(f"Generated wav file {call_wav_path} for call ID: {call_id}")
    wav_player.play_wav_file(call_wav_path)

    return jsonify({
        "call_id": call_id,
        "duration_seconds": audio_duration,
})


@app.route('/call_ended', methods=['POST'])
def call_ended():
    json_body = request.json
    sqlite_helper.end_call(call_id=json_body.get('call_id'))

    return jsonify({
        "status": "ok"
    })


if __name__ == "__main__":
    app.run(host=FLASK_HOST, port=FLASK_PORT, debug=True)
