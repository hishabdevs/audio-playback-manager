from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, Boolean, DateTime, ForeignKey, Float
from sqlalchemy.orm import relationship



Base = declarative_base()


class Call(Base):
    __tablename__ = 'calls'

    id = Column(Integer, primary_key=True, autoincrement=True)
    start_time = Column(DateTime, nullable=False)
    utterances = relationship(lambda: Utterance)
    duration_seconds = Column(Integer, nullable=True)
    is_finished = Column(Boolean, default=False)
    stop_time = Column(DateTime, nullable=True)


class Utterance(Base):
    __tablename__ = 'utterances'

    id = Column(Integer, primary_key=True, autoincrement=True)
    transcript = Column(String)
    wav_path = Column(String)
    wav_name = Column(String, nullable=True)
    duration_seconds = Column(Float, nullable=True)
    is_used = Column(Boolean, default=False)
    call_id = Column(Integer, ForeignKey(Call.id), nullable=True)


def create_tables_if_not_exists(engine):
    Base.metadata.create_all(engine)

