import glob
import logging
import os
from datetime import datetime

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
import arrow

from modules.db_helpers import models
from modules.utilities import audio_utils

MAX_ALLOWED_UTT_DURATION = 1 * 60  # SECONDS

engine = create_engine(f'sqlite:///{os.path.dirname(__file__)}/playback_manager.db', echo=True)
Session_obj = sessionmaker(bind=engine)
db_session = Session_obj()

models.create_tables_if_not_exists(engine)


def populate_utterances_from_wav_dir(wav_dir):
    wav_files = glob.glob(os.path.join(wav_dir, "*.wav"))
    print(f"Found {len(wav_files)} wav files in {wav_dir}.")

    for wav_file in wav_files:
        transcript_path = wav_file.replace(".wav", ".txt")
        with open(transcript_path, 'r') as f:
            transcript_text = f.read()

        utt = models.Utterance(wav_path=os.path.abspath(wav_file),
                               wav_name=os.path.basename(wav_file),
                               transcript=transcript_text,
                               duration_seconds=audio_utils.check_duration_of_wav(wav_file),
                               )
        db_session.add(utt)

    db_session.commit()


def get_utterances_for_a_call(max_utt_count=20):
    unused_utterances = db_session.query(models.Utterance).filter(models.Utterance.is_used.is_(False)).all()
    print(f"Total unused utterance loaded from DB: {len(unused_utterances)}")

    total_count = 1

    output = []
    for utt in unused_utterances:
        output.append(utt)

        if utt.duration_seconds > MAX_ALLOWED_UTT_DURATION:
            continue
        if total_count >= max_utt_count:
            break

        total_count += 1

    return output


def save_utterances_for_a_call(utterances):
    call = models.Call(start_time=datetime.utcnow(), )
    db_session.add(call)
    db_session.commit()
    print("call id: ", call.id)
    for utt in utterances:
        utt.is_used = True
        utt.call_id = call.id

    db_session.commit()
    return call.id


def update_call_duration_in_db(call_id, call_duration):
    call_obj = db_session.query(models.Call).filter(models.Call.id.is_(int(call_id))).first()
    call_obj.duration_seconds = call_duration
    db_session.commit()


def end_call(call_id):
    call_obj = db_session.query(models.Call).filter(models.Call.id.is_(int(call_id))).first()
    call_start_time_arrow = arrow.get(call_obj.start_time)
    ideal_call_end_time = call_start_time_arrow.shift(seconds=call_obj.duration_seconds)

    if arrow.now() >= ideal_call_end_time:
        call_obj.is_finished = True
    else:
        call_obj.is_finished = False
    call_obj.stop_time = arrow.now().datetime

    db_session.commit()



if __name__ == '__main__':
    populate_utterances_from_wav_dir("temp")
    # utts = get_utterances_for_a_call(max_utt_count=5)
    # call_id = save_utterances_for_a_call(utts)
    # print(call_id)
