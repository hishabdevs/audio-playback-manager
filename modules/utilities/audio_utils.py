from pydub import AudioSegment


def check_duration_of_wav(wav_path):
    audio = AudioSegment.from_wav(wav_path)
    return len(audio)/1000
