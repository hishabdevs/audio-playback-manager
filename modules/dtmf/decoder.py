#!/usr/bin/env python3

from pprint import pprint
import argparse
from typing import List

import numpy as np
from scipy.io import wavfile


# Most of the parts are taken from https://github.com/ribt/dtmf-decoder/blob/master/dtmf.py

dtmf_freq_matrix = {(697, 1209): "1", (697, 1336): "2", (697, 1477): "3", (770, 1209): "4", (770, 1336): "5", (770, 1477): "6",
                    (852, 1209): "7", (852, 1336): "8", (852, 1477): "9", (941, 1209): "*", (941, 1336): "0", (941, 1477): "#",
                    (697, 1633): "A", (770, 1633): "B", (852, 1633): "C", (941, 1633): "D"}


def __collapse_nearby_timestamps_for_same_digit__(timestamps: list) -> list:
    previous_digit = None
    previous_timestamp = None

    if len(timestamps) > 0:
        previous_digit = timestamps[0]['digit']
        previous_timestamp = 0
    else:
        return timestamps

    output = []
    # same_digit_continuing = False

    # merge same timestamps where digit is same as previous
    for ts in timestamps[1:]:
        this_digit = ts['digit']
        this_timestamp = ts['timestamp']

        # warning: for this condition, the parameter i should always be below 1 second
        if this_timestamp == previous_timestamp or this_timestamp == (previous_timestamp + 1):
            if not previous_digit == this_digit and not this_digit == '':
                output.append(ts)

        previous_digit = this_digit
        previous_timestamp = this_timestamp

    print("###############")
    print("First iter output:")
    pprint(output)
    print("###############")

    # sometimes a single frame detection error results in detecting a single digit as two separate ones. Check if any
    # two subsequent timestamps has a same digit.
    second_iter_output = []
    previous_digit = output[0]['digit']
    previous_timestamp = output[0]['timestamp']
    second_iter_output.append(output[0])

    for ts in output[1:]:
        this_digit = ts['digit']
        this_timestamp = ts['timestamp']

        if this_timestamp == (previous_timestamp + 1):
            if not previous_digit == this_digit:
                second_iter_output.append(ts)
        else:
            second_iter_output.append(ts)
        previous_digit = this_digit
        previous_timestamp = this_timestamp

    return second_iter_output


def get_timestamps_of_digits(wav_path: str, digits: List[str], precision: float = 0.04, acceptable_freq_error: int = 20):
    fps, data = wavfile.read(wav_path)

    duration = len(data) / fps
    step = int(len(data) // (duration // precision))

    output_timestamps = []

    for i in range(0, len(data) - step, step):
        signal = data[i:i + step]

        frequencies = np.fft.fftfreq(signal.size, d=1 / fps)
        amplitudes = np.fft.fft(signal)

        # Low
        i_min = np.where(frequencies > 0)[0][0]
        i_max = np.where(frequencies > 1050)[0][0]

        freq = frequencies[i_min:i_max]
        amp = abs(amplitudes.real[i_min:i_max])

        lf = freq[np.where(amp == max(amp))[0][0]]

        delta = acceptable_freq_error
        best = 0

        for f in [697, 770, 852, 941]:
            if abs(lf - f) < delta:
                delta = abs(lf - f)
                best = f

        lf = best

        # High
        i_min = np.where(frequencies > 1100)[0][0]
        i_max = np.where(frequencies > 2000)[0][0]

        freq = frequencies[i_min:i_max]
        amp = abs(amplitudes.real[i_min:i_max])

        hf = freq[np.where(amp == max(amp))[0][0]]

        delta = acceptable_freq_error
        best = 0

        for f in [1209, 1336, 1477, 1633]:
            if abs(hf - f) < delta:
                delta = abs(hf - f)
                best = f

        hf = best
        t = int(i // step * precision)
        m = str(int(t // 60))
        s = str(t % 60)
        s = "0" * (2 - len(s)) + s

        if lf == 0 or hf == 0:
            # no digit detected
            output_timestamps.append({"timestamp": t, "digit": "", "minute_second": f"{m}-{s}s"})
            continue
        elif dtmf_freq_matrix[(lf, hf)] is not None:
            c = dtmf_freq_matrix[(lf, hf)]
            output_timestamps.append({"timestamp": t, "digit": c, "minute_second": f"{m}-{s}s"})

    # pprint(output_timestamps)
    print('---------')
    output_timestamps = __collapse_nearby_timestamps_for_same_digit__(output_timestamps)
    pprint(output_timestamps)
    return output_timestamps


if __name__ == "__main__":
    get_timestamps_of_digits("sample_wavs/dtmf_call_short.wav", digits=['0', "9"])
    # get_timestamps_of_digits("1_dtmf_012391239123459.wav", digits=['0', "9"])
