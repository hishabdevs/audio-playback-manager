import os

from pydub import AudioSegment

DIGIT_0_AUDIOSEGMENT = AudioSegment.from_wav(os.path.join(os.path.dirname(__file__), "assets/0.wav"))
DIGIT_9_AUDIOSEGMENT = AudioSegment.from_wav(os.path.join(os.path.dirname(__file__), "assets/9_three_second.wav"))


def encode_utterances(utterances):
    """
    Adds DTMF digit 9 after every utterence (with one at the beginning)
    :param utterances:
    :return: pydub.AudioSegment
    """

    output = AudioSegment.empty() + DIGIT_9_AUDIOSEGMENT
    for utt in utterances:
        utt_segment = AudioSegment.from_wav(utt.wav_path)
        output += utt_segment + AudioSegment.silent(1000, frame_rate=8000)

    return output


def encode_call_id(spoken_call_id_audio_segment):
    """
    Generates an audio segment containing DTMF_0 (to mark start of a call) + caller_id
    :param spoken_call_id_audio_segment: Audio segment of the call ID
    :return: pydub.AudioSegment
    """
    return DIGIT_0_AUDIOSEGMENT + spoken_call_id_audio_segment
