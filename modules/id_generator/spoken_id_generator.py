import os

import pydub


DIGITS_WAV_DIR = os.path.join(os.path.dirname(__file__), "digits_wav")


def __pad_id_string_if_needed__(id_string: str, digit_count: int) -> str:
    if len(id_string) < digit_count:
        # zero padding needed
        padding = "".join(["0"] * (digit_count - len(id_string)))
        return padding + id_string
    else:
        # no padding needed
        return id_string


def __get_audio_segment_of_digit__(digit: str) -> pydub.AudioSegment:
    digit_wav_path = os.path.join(DIGITS_WAV_DIR, f"{digit}.wav")

    return pydub.AudioSegment.from_wav(digit_wav_path)


def generate_wav(id_integer: int, digit_count: int = 7) -> pydub.AudioSegment:
    """
    Generates wav from the given "id_integer" and returns a pydub.AudioSegment object containing a "digit_count" digit
    spoken ID (padded with leading zeros). For example: input "id" is 12345, padded spoken ID will be "0012345".

    It concatenates the spoken wav files in desired sequence.

    :return: pydub.AudioSegment
    """

    id_string = str(id_integer)
    padded_id_string = __pad_id_string_if_needed__(id_string=id_string, digit_count=digit_count)

    output_audio_segment = pydub.AudioSegment.empty()

    for digit in padded_id_string:
        output_audio_segment = output_audio_segment + __get_audio_segment_of_digit__(digit)

    return output_audio_segment


if __name__ == "__main__":
    from pydub.playback import play

    play(generate_wav(123405))
