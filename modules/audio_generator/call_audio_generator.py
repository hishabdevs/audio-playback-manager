import os

from modules.id_generator import spoken_id_generator
from modules.db_helpers import sqlite_helper
from modules.dtmf import encoder
from modules.utilities import audio_utils

CALL_WAV_OUTPUT_DIR = os.path.join(os.path.dirname(__file__), 'call_wavs')
os.makedirs(CALL_WAV_OUTPUT_DIR, exist_ok=True)


def generate_wav_file_for_call(max_utt_count=5):
    """
    This is the play_wav_file method to select utterances and generate audio for a call. It also adds necessary DTMFs and
    spoken IDs. The audio is structured like this -

    --DTMF 9--UTTERANCE 1--DTMF 9--UTTERANCE 2--DTMF 9--UTTERANCE N--DTMF 9-----

    To mark the beginning of a call, DTMF 0 is played at the start, followed by a 7 digit call ID in spoken form. 
    To mark the end of the call ID and start of "max_utt_count" utterances, a DTMF 9 tone is played.
     
    The utterances are less than sqlite_helper.MAX_ALLOWED_UTT_DURATION seconds each, in total "max_utt_count"
    utterances can be in a single call. Each utterance is separated by a DTMF 9 tone.
    
    :return: str, path of the output wav file 
    """

    utterances = sqlite_helper.get_utterances_for_a_call(max_utt_count=max_utt_count)
    call_id = sqlite_helper.save_utterances_for_a_call(utterances=utterances)
    print("Call ID: ", call_id)
    print(f"Total {len(utterances)} utterances selected for this call.")
    # todo check if no utt found

    utt_segment = encoder.encode_utterances(utterances=utterances)

    full_audio_segment_for_call = utt_segment

    output_wav_path = os.path.join(CALL_WAV_OUTPUT_DIR, f"{call_id}.wav")
    full_audio_segment_for_call.export(output_wav_path, format='wav')

    total_call_duration = audio_utils.check_duration_of_wav(output_wav_path)
    sqlite_helper.update_call_duration_in_db(call_id=call_id, call_duration=total_call_duration)

    print("Output wav: ", output_wav_path)
    return output_wav_path, call_id, total_call_duration


if __name__ == "__main__":
    wav = generate_wav_file_for_call()

