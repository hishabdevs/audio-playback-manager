import os
import time
import logging
import atexit
import sys

from pydub import AudioSegment
from pydub.playback import play
import psutil


pid_file_name = 'modules.pid'
pid_file_dir = os.path.dirname(os.path.abspath(__file__))

pid_file_path = os.path.join(pid_file_dir, pid_file_name)
pid_current = os.getpid()
is_pid_from_file_running = False

log = logging.getLogger()
stream_handler = logging.StreamHandler(sys.stdout)
log.addHandler(stream_handler)
log.setLevel(logging.DEBUG)


def __check_if_already_running__():
    if not os.path.exists(pid_file_path):
        return False

    with open(pid_file_path) as f:
        text = f.read()
        if text != '':
            pid = int(text.strip())
            running = psutil.pid_exists(pid)
            if running:
                logging.info(f'pid :{pid} already running.')
                return pid
            else:
                return None
        else:
            logging.error('Empty pid file encountered.')
            return None


def __create_pid_file__(pid):
    with open(pid_file_path,'w') as f:
        f.write(str(pid))


def __clear_pid_file__():
    os.remove(pid_file_path)

#
# @atexit.register
# def __shutdown__():
#     if not is_pid_from_file_running:
#         __clear_pid_file__()
#     logging.info('Shutting down modules.')
#
#     # if running in windows
#     try:
#         import winsound
#         winsound.PlaySound(None, winsound.SND_ASYNC)
#     except:
#         pass
#
#     # if running in linux


# 01798405060

def play_wav_file(wav_path):
    # running = __check_if_already_running__()
    # if not running:
    #     __create_pid_file__()
    # else:
    #     exit(1)
    logging.info('Playing file:' + wav_path)

    # try:
    #     winsound.PlaySound(wav_path, winsound.SND_ASYNC | winsound.SND_ALIAS)
    # except:

    import subprocess

    already_running_pid = __check_if_already_running__()
    print(already_running_pid)
    if already_running_pid:
        # kill already running process and clear pid file
        # logging.info("Already running PID: ", already_running_pid)
        os.kill(already_running_pid, 9)
        logging.info(f"Killed pid: {already_running_pid}")
        __clear_pid_file__()

    process = subprocess.Popen(["ffplay", wav_path, '-nodisp'], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    pid = process.pid
    __create_pid_file__(pid)
    logging.info("Returing method play_wav_file")



if __name__ == '__main__':
    play_wav_file("test.wav")
